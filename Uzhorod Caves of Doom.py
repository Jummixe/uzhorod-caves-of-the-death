#!/usr/bin/python

import libtcodpy as libtcod
import math
import textwrap
import shelve
import copy
import random
import abc
Player=0
#actual size of the window
SCREEN_WIDTH = 80
SCREEN_HEIGHT = 50

#size of the map
MAP_WIDTH = 80
MAP_HEIGHT = 43

#sizes and coordinates relevant for the GUI
BAR_WIDTH = 20
PANEL_HEIGHT = 7
PANEL_Y = SCREEN_HEIGHT - PANEL_HEIGHT
MSG_X = BAR_WIDTH + 2
MSG_WIDTH = SCREEN_WIDTH - BAR_WIDTH - 2
MSG_HEIGHT = PANEL_HEIGHT - 1
INVENTORY_WIDTH = 50
CHARACTER_SCREEN_WIDTH = 30
LEVEL_SCREEN_WIDTH = 40

#parameters for dungeon generator
ROOM_MAX_SIZE = 9
ROOM_MIN_SIZE = 4
MAX_ROOMS = 30

#spell values
HEAL_AMOUNT = 40
LIGHTNING_DAMAGE = 40
LIGHTNING_RANGE = 5
CONFUSE_RANGE = 8
CONFUSE_NUM_TURNS = 10
FIREBALL_RADIUS = 3
FIREBALL_DAMAGE = 25
effects = []
#experience and level-ups
LEVEL_UP_BASE = 200
LEVEL_UP_FACTOR = 150


FOV_ALGO = 0  #default FOV algorithm
FOV_LIGHT_WALLS = True  #light walls or not
TORCH_RADIUS = 10

LIMIT_FPS = 20  #20 frames-per-second maximum

color_dark_wall = libtcod.Color(50, 52, 50)
color_light_wall = libtcod.Color(110, 100, 101)
color_dark_ground = libtcod.Color(70, 70, 70)
color_light_ground = libtcod.Color(190, 190, 90)
class Tile:
    #a tile of the map and its properties
    def __init__(self, blocked, block_sight = None):
        self.blocked = blocked

        #all tiles start unexplored
        self.explored = False

        #by default, if a tile is blocked, it also blocks sight
        if block_sight is None: block_sight = blocked
        self.block_sight = block_sight

class Rect:
    #a rectangle on the map. used to characterize a room.
    def __init__(self, x, y, w, h):
        self.x1 = x
        self.y1 = y
        self.x2 = x + w
        self.y2 = y + h

    def center(self):
        center_x = (self.x1 + self.x2) / 2
        center_y = (self.y1 + self.y2) / 2
        return (center_x, center_y)

    def intersect(self, other):
        #returns true if this rectangle intersects with another one
        return (self.x1 <= other.x2 and self.x2 >= other.x1 and
                self.y1 <= other.y2 and self.y2 >= other.y1)

class Object:
    #this is a generic object: the player, a monster, an item, the stairs...
    #it's always represented by a character on screen.
    def __init__(self, x, y, char, name, color, blocks=False,ammo = False, effect = None, always_visible=False, fighter=None, ai=None, item=None, equipment=None, wand = None,Description=None):
        self.x = x
        self.y = y
        self.char = char
        self.name = name
        self.color = color
        self.blocks = blocks
        self.always_visible = always_visible
        self.fighter = fighter
        self.effect = effect
        self.ammo = ammo
        if self.fighter:  #let the fighter component know who owns it
            self.fighter.owner = self
        self.ai = ai
        if self.ai:  #let the AI component know who owns it
            self.ai.owner = self
        if self.effect:
            self.effect.owner = self
        self.item = item
        if self.item:  #let the Item component know who owns it
            self.item.owner = self
        self.wand = wand
        self.equipment = equipment
        if self.equipment:  #let the Equipment component know who owns it
            self.equipment.owner = self

            #there must be an Item component for the Equipment component to work properly
            self.item = Item(1)
            self.item.owner = self
        if self.wand:
            self.wand.owner = self

    def move(self, dx, dy):
        #move by the given amount, if the destination is not blocked
        if not is_blocked(self.x + dx, self.y + dy):
            self.x += dx
            self.y += dy

    def move_towards(self, target_x, target_y):
        #vector from this object to the target, and distance
        dx = target_x - self.x
        dy = target_y - self.y
        distance = math.sqrt(dx ** 2 + dy ** 2)

        #normalize it to length 1 (preserving direction), then round it and
        #convert to integer so the movement is restricted to the map grid
        dx = int(round(dx / distance))
        dy = int(round(dy / distance))
        self.move(dx, dy)

    def distance_to(self, other):
        #return the distance to another object
        dx = other.x - self.x
        dy = other.y - self.y
        return math.sqrt(dx ** 2 + dy ** 2)

    def distance(self, x, y):
        #return the distance to some coordinates
        return math.sqrt((x - self.x) ** 2 + (y - self.y) ** 2)

    def send_to_back(self):
        #make this object be drawn first, so all others appear above it if they're in the same tile.
        global objects
        objects.remove(self)
        objects.insert(0, self)

    def draw(self):
        #only show if it's visible to the player; or it's set to "always visible" and on an explored tile
        if (libtcod.map_is_in_fov(fov_map, self.x, self.y) or
                (self.always_visible and map[self.x][self.y].explored)):
            #set the color and then draw the character that represents this object at its position
            libtcod.console_set_default_foreground(con, self.color)
            libtcod.console_put_char(con, self.x, self.y, self.char, libtcod.BKGND_NONE)

    def clear(self):
        #erase the character that represents this object
        libtcod.console_put_char(con, self.x, self.y, ' ', libtcod.BKGND_NONE)

class Effect:

    def __init__(self,char,duration):
        self.char = char
        self.duration = duration

    def continues(self):
        if self.continues>0:
            self.continues=self.continues-1
        else:
            effects.remove(self.owner)

def distance_to_a(x,y, other):
        #return the distance to another object
        dx = other.x - x
        dy = other.y - y
        return math.sqrt(dx ** 2 + dy ** 2)




class Fighter:
    #combat-related properties and methods (monster, player, NPC).
    def __init__(self, hp, defense, power, xp,default_speed=0,speed=100,death_function=None,hungry=None,path = None,inventory=[]):
        self.base_max_hp = hp
        self.hp = hp
        self.base_defense = defense
        self.base_power = power
        self.xp = xp
        self.death_function = death_function
        self.hungry = hungry
        self.speed = speed
        self.default_speed = speed
        self.path = path
        if inventory == []:
            self.inventory = []
        else:
            self.inventory=inventory
        print(self.inventory)
    @property
    def speed(self):
        bonus = sum(equipment.power_bonus for equipment in get_all_equipped(self.owner))
        return self.speed + bonus

    @property
    def power(self):  #return actual power, by summing up the bonuses from all equipped items
        bonus = sum(equipment.power_bonus for equipment in get_all_equipped(self.owner))
        return self.base_power + bonus

    @property
    def defense(self):  #return actual defense, by summing up the bonuses from all equipped items
        bonus = sum(equipment.defense_bonus for equipment in get_all_equipped(self.owner))
        return self.base_defense + bonus

    @property
    def max_hp(self):  #return actual max_hp, by summing up the bonuses from all equipped items
        bonus = sum(equipment.max_hp_bonus for equipment in get_all_equipped(self.owner))
        return self.base_max_hp + bonus

    def every_turn(self):
       if self.hungry:
        #self.hungry -= 1
        if self.hungry<=50 and self.hungry>=25:
            message('You feel somewhat hungry')
        elif self.hungry<=25:
            message('You Feel really hungry!',libtcod.red)
        if self.hungry <= 0:
            function = self.death_function
            if function is not None:
                    function(self.owner)

    def attack(self, target,attack_type='attacks'):
        #a simple formula for attack damage
        damage = self.power - target.fighter.defense

        if damage > 0:
            #make the target take some damage
            message(self.owner.name.capitalize() +' ' + attack_type+' ' + target.name + ' for ' + str(damage) + ' hit points.')
            target.fighter.take_damage(damage)
        else:
            message(self.owner.name.capitalize() +' ' + attack_type + ' ' +target.name + ' but it has no effect!')

    def take_damage(self, damage):
        #apply damage if possible
        if damage > 0:
            self.hp -= damage

            #check for death. if there's a death function, call it
            if self.hp <= 0:
                function = self.death_function
                if function is not None:
                    function(self.owner)

                if self.owner != player:  #yield experience to the player
                    player.fighter.xp += self.xp

    def heal(self, amount):
        #heal by the given amount, without going over the maximum
        self.hp += amount
        if self.hp > self.max_hp:
            self.hp = self.max_hp

def path_func_return(xFrom, yFrom, xTo, yTo, userdata):
    if map[xTo][yTo].blocked:
        return 0.0
    #now check for any blocking objects
    for object in objects:
        if object.blocks and object.x == xTo and object.y == yTo and object  != player:
            return 0.0
    return 1.0

class BasicMonster:
    def __init__(self):
        self.path=None
        self.alert=None
    def take_turn(self):
        #a basic monster takes its turn. if you can see it, it can see you
            monster = self.owner
            monster.fighter.speed-=100
            if libtcod.map_is_in_fov(fov_map, monster.x, monster.y) or self.alert == True:
              #move towards player if far away
              if self.alert != True:
                  self.alert = True
              if monster.distance_to(player) >=2:
               if not line(monster.x,monster.y,player.x,player.y).compute_towards():
                    monster.ai.path = libtcod.path_new_using_function(80,43,path_func_return, userdata=0, dcost=1.41)
                    if libtcod.path_compute(self.path,monster.x,monster.y,player.x,player.y):
                         monster.ai.walk_path()
               elif  line(monster.x,monster.y,player.x,player.y).compute_towards():
                    monster.move_towards(player.x,player.y)

              #close enough, attack! (if the player is still alive.)
              elif monster.fighter.hp < monster.fighter.base_max_hp/3:
                   monster.move_towards(monster.x-monster.distance_to(player),monster.y-monster.distance_to(player))
              #Flee if his hp is very low.
              elif player.fighter.hp > 0:
                  monster.fighter.attack(player)
    def walk_path(self):
      print(self.path)
      monster=self.owner
      x=1
      y=1
      x,y=libtcod.path_walk(self.path,True)
      if (x,y) != (player.x,player.y):
          print('ya poshel')
          monster.x=x
          monster.y=y

class CleverMonster:

    def __init__(self):
        self.path=None
        self.alert=None
    def take_turn(self):
        #a basic monster takes its turn. if you can see it, it can see you
            monster = self.owner
            monster.fighter.speed-=100
            if self.alert != True:
                for i in objects:
                      if i.item:
                        if i.x==monster.x and i.y==monster.y:
                           i.item.pick_up_simple(monster.fighter)
                           message('You will not believe... But i found something! '+i.name+' '+monster.name)
                           return
                monster.move(libtcod.random_get_int(0, -1, 1), libtcod.random_get_int(0, -1, 1))
            if libtcod.map_is_in_fov(fov_map, monster.x, monster.y) or self.alert == True:
              #move towards player if far away
              for i in monster.fighter.inventory:
                     for j in offensive_funcs:
                        if i.item.use_function:
                         print (i.item.use_function.__name__+' TUT SMOTRI'+j.__name__)
                         if i.item.use_function.__name__ ==j.__name__ :
                            message('Hey you, i will use something to beat you up ')
                            i.item.use()
                            message('I will use something!'+i.name+' '+monster.name+' ')
                            return
              if self.alert != True:
                  self.alert = True
              if monster.distance_to(player) >=2:

               if not line(monster.x,monster.y,player.x,player.y).compute_towards():
                    monster.ai.path = libtcod.path_new_using_function(80,43,path_func_return, userdata=0, dcost=1.41)
                    if libtcod.path_compute(self.path,monster.x,monster.y,player.x,player.y):
                         monster.ai.walk_path()
               elif  line(monster.x,monster.y,player.x,player.y).compute_towards():
                    monster.move_towards(player.x,player.y)

              #close enough, attack! (if the player is still alive.)
              elif monster.fighter.hp < monster.fighter.base_max_hp/4:
                   for i in monster.fighter.inventory:

                         if i.item.use_function in defense_funcs:
                            i.item.use()
                            message('I will use something!'+i.name+' '+monster.name+' '+str(monster.fighter.hp))
                   monster.move_towards(monster.x-5,monster.y-libtcod.random_get_int(0,-5,5))
              #Fleeing if your hp is very low.
              elif player.fighter.hp > 0:
                  monster.fighter.attack(player)
    def walk_path(self):
      print(self.path)
      monster=self.owner
      x=1
      y=1
      x,y=libtcod.path_walk(self.path,True)
      if (x,y) != (player.x,player.y):
          print('ya poshel')
          monster.x=x
          monster.y=y


class RangerMonster:

    def __init__(self):
        self.alert =False
    def can_see(self):
        monster = self.owner
        print(line(monster.x,monster.y,player.x,player.y).compute_towards())
        if line(monster.x,monster.y,player.x,player.y).compute_towards() and monster.distance_to(player)<=9:
            print('yoba')
            return True
    def take_turn(self):
        #a basic monster takes its turn. if you can see it, it can see you
            monster = self.owner
            self.owner.fighter.speed-=100
            if libtcod.map_is_in_fov(fov_map, monster.x, monster.y) or self.alert == True:
              #move towards player if far away
              if self.alert != True:
                  self.alert = True
              if not self.can_see():
                print('hell yeah')
                monster.ai.path = libtcod.path_new_using_function(80,43,path_func_return, userdata=0, dcost=1.41)
                if libtcod.path_compute(self.path,monster.x,monster.y,player.x,player.y):
                    monster.ai.walk_path()
              #close enough, attack! (if the player is still alive.)
              elif player.fighter.hp > 0:
                  monster.fighter.attack(player,'shoots')
    def walk_path(self):
      print(self.path)
      monster=self.owner
      x=1
      y=1
      x,y=libtcod.path_walk(self.path,True)
      if (x,y) != (player.x,player.y):
              monster.x=x
              monster.y=y

#MOVE OR ATTACK TAKES EXACTLY 100 POINTS
class ConfusedMonster:
    #AI for a temporarily confused monster (reverts to previous AI after a while).
    def __init__(self, old_ai, num_turns=CONFUSE_NUM_TURNS):
        self.old_ai = old_ai
        self.num_turns = num_turns

    def take_turn(self):
        if self.num_turns > 0:  #still confused...
            #move in a random direction, and decrease the number of turns confused
            self.owner.move(libtcod.random_get_int(0, -1, 1), libtcod.random_get_int(0, -1, 1))
            self.num_turns -= 1

        else:  #restore the previous AI (this one will be deleted because it's not referenced anymore)
            self.owner.ai = self.old_ai
            message('The ' + self.owner.name + ' is no longer confused!', libtcod.red)
        self.owner.fighter.speed-=1



class Item:
    #an item that can be picked up and used.

    def __init__(self,count=1,use_function=None,countable = False,who_owns = None):
        self.use_function = use_function
        self.count = count
        self.countable = countable
        self.who_owns = who_owns

    def pick_up_simple(self,who):

        #Equipment attribute
        equipment = self.owner.equipment
        #Wand attribute
        wand = self.owner.wand
        #add to the player's inventory and remove from the map
        item = self.owner
        if len(who.inventory) >= 26:
            return
        else:
         if not any(i.name == item.name for i in who.inventory) or not self.owner.item.countable:
             who.inventory.append(item)
             self.who_owns=who
             self.who_owns.speed-=100
         else:
            for i in  who.inventory:
                print(i.item.count)
                if item.name == i.name:
                    i.item.count+=item.item.count
                    break
         if equipment and get_equipped_in_slot(equipment.slot,who) is None:
                equipment.equip()
         objects.remove(self.owner)
            #special case: automatically equip, if the corresponding equipment slot is unused


    def pick_up(self,who=None):

        if who==None:
            who=player.fighter
        #Equipment attribute
        equipment = self.owner.equipment
        #Wand attribute
        wand = self.owner.wand
        #add to the player's inventory and remove from the map
        item = self.owner

        if len(who.inventory) >= 26 and who==player.fighter:
            message('Your inventory is full, cannot pick up ' + self.owner.name + '.', libtcod.red)
        else:
         if not any(i.name == item.name for i in who.inventory) or not self.owner.item.countable:
             who.inventory.append(item)
             self.who_owns=who
             self.who_owns.speed-=100
         else:
            for index, i in enumerate(who.inventory):
                print(i.item.count)
                if i.name == item.name:
                    i.item.count+=item.item.count
                    break
         objects.remove(self.owner)
         message('You picked up a ' + self.owner.name + '!', libtcod.green)
            #special case: automatically equip, if the corresponding equipment slot is unused
         if equipment and get_equipped_in_slot(equipment.slot,who) is None:
                equipment.equip()

    def check_count(self):
          if self.count<=0 and self.who_owns!=None:
           print(self.who_owns)
           self.who_owns.inventory.remove(self.owner)
          elif self.who_owns == None:
            self.remove
    def drop(self):
        #special case: if the object has the Equipment component, dequip it before dropping
        if self.owner.equipment:
            self.owner.equipment.dequip()
        #add to the map and remove from the player's inventory. also, place it at the player's coordinates
        self.count-=1
        item = copy.deepcopy(self.owner)
        item.x = player.x
        item.y = player.y
        item.count = 1
        self.check_count()
        if sort_tile(item.x,item.y,item) == False:
         self.who_owns=None
         objects.append(item)
        message('You dropped a ' + self.owner.name + '.', libtcod.yellow)

    def use(self):
        #special case: if the object has the Equipment component, the "use" action is to equip/dequip
        self.who_owns.speed-=100
        if self.owner.equipment:
            self.owner.equipment.toggle_equip()
            return
        if self.owner.wand:
          if self.owner.wand.check_wand_charge()==True:
             self.owner.wand.charges-=1
             message('Wand')
          else:
             message('You cannot zap this wand!',libtcod.red)
             return
        #just call the "use_function" if it is defined
        if self.use_function is None:
            message('The ' + self.owner.name + ' cannot be used.')
        else:
            if self.use_function(self.who_owns) != 'cancelled':
               print(str(self.count)+'County county dwaaarf!')
               if self.countable:
                self.count = self.count - 1
                self.check_count()
            print(str(self.count)+'County county dwaaarf 3 !')

class wand():

    def __init__(self,max_charges,charges):
        self.max_charges= charges
        self.charges = charges

    def check_wand_charge(self):
        if self.charges ==0:
            return False
        else:
            return True

class Effect:
    def __init__(self,duration):
        self.duration = duration
    def continues(self):
        if self.duration>0:
            self.duration -= 1
        else:
            effects.remove(self.owner)

#an object that can be equipped, yielding bonuses. automatically adds the Item component.
class ranged:

    def __init__(self,prepared=False,ammo = None,ammo_type = None,who_owns = None ):
        self.prepared = prepared
        self.ammo = ammo
        self.ammo_type = ammo_type
        self.who_owns = who_owns
    def prepare_ranged(self):
        if not any(i.name == self.ammo_type for i in self.who_owns.inventory) :
            return False
        else:
            for i in self.who_owns.inventory:
                if i.name == self.ammo_type:
                  self.ammo= i
                  return True

    def fire(self):
        if self.prepare_ranged():
                message('Please choose targer',libtcod.yellow)
                (x, y) = target_tile()
                if x is None:return
                linefunc = line(player.x,self.who_owns.y,x,y)
                if linefunc.compute() == True:
                    for obj in objects:
                      if obj.x == linefunc.stuck_x and obj.y == linefunc.stuck_y and obj.fighter:
                         message('The '+obj.name + ' damaged for ' + str(self.owner.power_bonus)+ 'hit points.',libtcod.orange)
                         obj.fighter.take_damage(self.owner.power_bonus)
                    message('you shoots an arrow!')
                    shoot =   copy.deepcopy(self.ammo)
                    shoot = copy.deepcopy(self.ammo)
                    shoot.x = linefunc.stuck_x
                    shoot.y = linefunc.stuck_y
                    shoot.item.count = 1
                    if shoot.item.count !=1:
                        return

                    weapon = get_equipped_in_slot('right hand',self.owner.who_owns)
                    if weapon != None:
                        player.fighter.speed-=weapon.equipment.speed
                    self.ammo.item.count-=1
                    pew = shoot
                    pew.item.count = 1
                    objects.append(pew)
                    self.ammo.item.check_count()
                    self.who_owns.fighter.speed-=self.owner.speed
                else:
                    return
        else:
            return

class Equipment:

    def __init__(self, slot, power_bonus=0, defense_bonus=0, max_hp_bonus=0,bonuses = [],speed=100,ranged = None,who_owns=None):
        self.power_bonus = power_bonus
        self.defense_bonus = defense_bonus
        self.max_hp_bonus = max_hp_bonus
        self.bonuses = bonuses
        #Action-point value, if bigger than slower weapon!
        self.speed = speed
        #Slot on your body
        self.ranged = ranged
        self.slot = slot
        self.is_equipped = False
        if self.ranged:
            self.ranged.owner = self

    def toggle_equip(self):  #toggle equip/dequip status
        if self.is_equipped:
            self.dequip()
        else:
            self.equip()

    def equip(self):
        #if the slot is already being used, dequip whatever is there firs
        old_equipment = get_equipped_in_slot(self.slot,self.owner.item.who_owns)
        hands_equipment = get_equipped_in_slot('hands',self.owner.item.who_owns)
        if old_equipment is not None:
           old_equipment.equipment.dequip()
        #equip object and show a message about it
        self.is_equipped = True
        message('Equipped ' + self.owner.name + ' on ' + self.slot + '.', libtcod.light_green)
        self.owner.item.who_owns.speed-=100
    def dequip(self):
        #dequip object and show a message about it
        if not self.is_equipped: return
        self.is_equipped = False
        message('Dequipped ' + self.owner.name + ' from ' + self.slot + '.', libtcod.light_yellow)
        print(self.owner.item.who_owns.hp)
        self.owner.item.who_owns.speed-=100
def sort_tile(x,y,what):
    #Sort things by their count, to avoid one-object stacks
    #DONT FORGET TO IMPROVEEE!!!!
    for i in objects:
      if i.item:
        if i.x==x and i.y==y and i.name == what.name and i.item.countable:
            i.item.count+=what.item.count
            return True
    return False

def get_equipped_in_slot(slot,obj):  #returns the equipment in a slot, or None if it's empty
    for obj in obj.inventory:
        if obj.equipment and obj.equipment.slot == slot and obj.equipment.is_equipped:
            return obj
    return None

def get_equipped_ranged(obj):
    for item in obj.fighter.inventory:
        if item.equipment and item.equipment.ranged==True and item.equipment.is_equipped:
            return item
    return None

def get_all_equipped(obj):  #returns a list of equipped items
    if obj.fighter.inventory is not None:
        equipped_list = []
        for item in obj.fighter.inventory:
            if item.equipment and item.equipment.is_equipped:
                equipped_list.append(item.equipment)
        return equipped_list
    else:
        return []
def is_blocked(x, y):
    #first test the map tile
    if map[x][y].blocked:
        return True
    #now check for any blocking objects
    for object in objects:
        if object.blocks and object.x == x and object.y == y:
            return True
    return False

def create_room(room):
    global map
    #go through the tiles in the rectangle and make them passable
    for x in range(room.x1 + 1, room.x2):
        for y in range(room.y1 + 1, room.y2):
            map[x][y].blocked = False
            map[x][y].block_sight = False

def create_h_tunnel(x1, x2, y):
    global map
    #horizontal tunnel. min() and max() are used in case x1>x2
    for x in range(min(x1, x2), max(x1, x2) + 1):
        map[x][y].blocked = False
        map[x][y].block_sight = False

def create_v_tunnel(y1, y2, x):
    global map
    #vertical tunnel
    for y in range(min(y1, y2), max(y1, y2) + 1):
        map[x][y].blocked = False
        map[x][y].block_sight = False

def make_entrance_map():
    global map, objects, stairs

    #the list of objects with just the player
    objects = [player]

    #fill map with "blocked" tiles
    map = [[ Tile(True)
             for y in range(MAP_HEIGHT) ]
           for x in range(MAP_WIDTH) ]

    rooms = []
    num_rooms = 0

    for r in range(10):
        #random width and height
        if r==0:
            w = 10
            h = 35
            #random position without going out of the boundaries of the map
            x = 2
            y = 2
        else:
            w = libtcod.random_get_int(0, ROOM_MIN_SIZE, ROOM_MAX_SIZE)
            h = libtcod.random_get_int(0, ROOM_MIN_SIZE, ROOM_MAX_SIZE)
            #random position without going out of the boundaries of the map
            x = libtcod.random_get_int(0, 0, MAP_WIDTH - w - 1)
            y = libtcod.random_get_int(0, 0, MAP_HEIGHT - h - 1)

        #"Rect" class makes rectangles easier to work with
        new_room = Rect(x, y, w, h)

        #run through the other rooms and see if they intersect with this one
        failed = False
        for other_room in rooms:
            if new_room.intersect(other_room):
                failed = True
                break

        if not failed:
            #this means there are no intersections, so this room is valid

            #"paint" it to the map's tiles
            create_room(new_room)

            #add some contents to this room, such as monsters
            place_objects(new_room)

            #center coordinates of new room, will be useful later
            (new_x, new_y) = new_room.center()

            if num_rooms == 0:
                #this is the first room, where the player starts at
                player.x = new_x
                player.y = new_y
            else:
                #all rooms after the first:
                #connect it to the previous room with a tunnel

                #center coordinates of previous room
                (prev_x, prev_y) = rooms[num_rooms-1].center()

                #draw a coin (random number that is either 0 or 1)
                if libtcod.random_get_int(0, 0, 1) == 1:
                    #first move horizontally, then vertically
                    create_h_tunnel(prev_x, new_x, prev_y)
                    create_v_tunnel(prev_y, new_y, new_x)
                else:
                    #first move vertically, then horizontally
                    create_v_tunnel(prev_y, new_y, prev_x)
                    create_h_tunnel(prev_x, new_x, new_y)

            #finally, append the new room to the list
            rooms.append(new_room)
            num_rooms += 1
    new_room = Rect(2, (MAP_HEIGHT/2)-7, MAP_WIDTH-10, 3)
    create_room(new_room)
    new_room = Rect(MAP_WIDTH-12, (MAP_HEIGHT/2)-14, 10, 20)
    create_room(new_room)
    #create stairs at the center of the last room
    stairs = Object(MAP_WIDTH-6, (MAP_HEIGHT/2)-7, '<', 'stairs', libtcod.white, always_visible=True)
    objects.append(stairs)
    stairs.send_to_back()  #so it's drawn below the monsters

def make_map():
    global map, objects, stairs

    #the list of objects with just the player
    objects = [player]

    #fill map with "blocked" tiles
    map = [[ Tile(True)
             for y in range(MAP_HEIGHT) ]
           for x in range(MAP_WIDTH) ]

    rooms = []
    num_rooms = 0

    for r in range(MAX_ROOMS):
        #random width and height
        w = libtcod.random_get_int(0, ROOM_MIN_SIZE, ROOM_MAX_SIZE)
        h = libtcod.random_get_int(0, ROOM_MIN_SIZE, ROOM_MAX_SIZE)
        #random position without going out of the boundaries of the map
        x = libtcod.random_get_int(0, 0, MAP_WIDTH - w - 1)
        y = libtcod.random_get_int(0, 0, MAP_HEIGHT - h - 1)

        #"Rect" class makes rectangles easier to work with
        new_room = Rect(x, y, w, h)

        #run through the other rooms and see if they intersect with this one
        failed = False
        for other_room in rooms:
            if new_room.intersect(other_room):
                failed = True
                break

        if not failed:
            #this means there are no intersections, so this room is valid

            #"paint" it to the map's tiles
            create_room(new_room)

            #add some contents to this room, such as monsters
            place_objects(new_room)

            #center coordinates of new room, will be useful later
            (new_x, new_y) = new_room.center()

            if num_rooms == 0:
                #this is the first room, where the player starts at
                player.x = new_x
                player.y = new_y
            else:
                #all rooms after the first:
                #connect it to the previous room with a tunnel

                #center coordinates of previous room
                (prev_x, prev_y) = rooms[num_rooms-1].center()

                #draw a coin (random number that is either 0 or 1)
                if libtcod.random_get_int(0, 0, 1) == 1:
                    #first move horizontally, then vertically
                    create_h_tunnel(prev_x, new_x, prev_y)
                    create_v_tunnel(prev_y, new_y, new_x)
                else:
                    #first move vertically, then horizontally
                    create_v_tunnel(prev_y, new_y, prev_x)
                    create_h_tunnel(prev_x, new_x, new_y)

            #finally, append the new room to the list
            rooms.append(new_room)
            num_rooms += 1

    #create stairs at the center of the last room
    stairs = Object(new_x, new_y, '<', 'stairs', libtcod.white, always_visible=True)
    objects.append(stairs)
    stairs.send_to_back()  #so it's drawn below the monsters

def random_choice_index(chances):  #choose one option from list of chances, returning its index
    #the dice will land on some number between 1 and the sum of the chances
    dice = libtcod.random_get_int(0, 1, sum(chances))

    #go through all chances, keeping the sum so far
    running_sum = 0
    choice = 0
    for w in chances:
        running_sum += w

        #see if the dice landed in the part that corresponds to this choice
        if dice <= running_sum:
            return choice
        choice += 1

def some_objects_spawn(x,y,spawn_what,count):
    for i in range(0,count):
     xs=libtcod.random_get_int(0,x+1,x-1)
     ys=libtcod.random_get_int(0,y+1,y-1)
     if xs>0 and xs<80 and ys>0 and ys<50:
      if not is_blocked(xs,ys):
        spawn_what_copy = copy.deepcopy(spawn_what)
        spawn_what_copy.x = xs
        spawn_what_copy.y = ys
        objects.append(spawn_what_copy)

def random_choice(chances_dict):
    #choose one option from dictionary of chances, returning its key
    chances = chances_dict.values()
    strings = chances_dict.keys()

    return strings[random_choice_index(chances)]

def from_dungeon_level(table):
    #returns a value that depends on level. the table specifies what value occurs after each level, default is 0.
    for (value, level) in reversed(table):
        if dungeon_level >= level:
            return value
    return 0

def place_objects(room):
    #this is where we decide the chance of each monster or item appearing.

    #maximum number of monsters per room
    max_monsters = from_dungeon_level([[2, 1], [3, 4], [5, 6]])

    #chance of each monster
    monster_chances = {}
    monster_chances['goblin'] = from_dungeon_level([[100, 1], [10, 5], [50, 10]]) #orc always shows up, even if all other monsters have 0 chance
    monster_chances['troll'] = from_dungeon_level([[15, 3], [30, 5], [60, 7]])
    monster_chances['rat'] = 70
    monster_chances['orc-warlord'] = from_dungeon_level([[0,0],[15,2],[30,5]])
    monster_chances['wolf'] = from_dungeon_level([[15,2],[50,4],[60,7]])
    monster_chances['skolopendra'] = from_dungeon_level([[15,2],[20,4],[40,7]])
    monster_chances['goblin_archer'] = from_dungeon_level([[20,1],[40,7]])
    monster_chances['goblin_group'] = from_dungeon_level([[1,1],[50,5]])
    #maximum number of items per room
    max_items = from_dungeon_level([[1, 1], [2, 4]])

    #chance of each item (by default they have a chance of 0 at level 1, which then goes up)
    item_chances = {}
    item_chances['heal'] = 55  #healing potion always shows up, even if all other items have 0 chance
    item_chances['lightning'] = from_dungeon_level([[50, 1]])
    item_chances['fireball'] =  from_dungeon_level([[50, 1]])
    item_chances['confuse'] =   from_dungeon_level([[10, 3]])
    item_chances['sword'] =     from_dungeon_level([[1, 1]])
    item_chances['shield'] =    from_dungeon_level([[1, 1]])
    item_chances['cap'] =  from_dungeon_level([[1,2]])
    item_chances['leather armor'] = from_dungeon_level([[1,2]])
    item_chances['boots'] = from_dungeon_level([[1,1]])
    item_chances['wand of flame'] = from_dungeon_level([[1,2]])
    item_chances['arrow'] = from_dungeon_level([[5,1]])
    item_chances['bow'] = from_dungeon_level([[1,1]])
    #choose random number of monsters
    num_monsters = libtcod.random_get_int(0, 0, max_monsters)

    for i in range(num_monsters):
        #choose random spot for this monster
        x = libtcod.random_get_int(0, room.x1+1, room.x2-1)
        y = libtcod.random_get_int(0, room.y1+1, room.y2-1)

        #only place it if the tile is not blocked
        if not is_blocked(x, y):
            choice = random_choice(monster_chances)
            if choice == 'goblin':
                #create an orc
                monster=create_random()
                monster.x=x
                monster.y=y
                print(monster.name+' yo')
            elif choice == 'troll':
                #create a troll
                fighter_component = Fighter(hp=30, defense=2, power=8, xp=100, death_function=monster_death)
                ai_component = BasicMonster()

                monster = Object(x, y, 'T', 'troll', libtcod.darker_green,
                                 blocks=True, fighter=fighter_component, ai=ai_component)

            elif choice == 'goblin_archer':
                #create a troll
                fighter_component = Fighter(hp=15, defense=0, power=3, xp=100, death_function=monster_death)
                ai_component = RangerMonster()

                monster = Object(x, y, 'g', 'goblin slinger', libtcod.darker_red,
                blocks=True, fighter=fighter_component, ai=ai_component )

            elif choice == 'wolf':
                #create a wolf
                fighter_component = Fighter(hp=12, defense=0, power=6, xp=100, death_function=monster_death)
                ai_component = BasicMonster()

                monster = Object(x, y, 'w', 'wolf', libtcod.darker_gray,
                                 blocks=True, fighter=fighter_component, ai=ai_component)
                some_objects_spawn(x,y,monster,3)

            elif choice == 'skolopendra':
                #create a wolf
                fighter_component = Fighter(hp=5, defense=2, power=9, xp=200, death_function=monster_death)
                ai_component = BasicMonster()

                monster = Object(x, y, 'Q', 'skolopendra', libtcod.darker_red,
                                 blocks=True, fighter=fighter_component, ai=ai_component)
                some_objects_spawn(x,y,monster,3)

            elif choice == 'rat':
                #create a rat
                equipment_component = Equipment(slot='right hand', power_bonus=2)

                fighter_component = Fighter(hp=7, defense=1, power=2, xp=15,default_speed=125,speed=125,death_function=monster_death)

                ai_component = BasicMonster()


                monster = Object(x, y, 'r', 'rat',libtcod.gray,
                                 blocks=True, fighter=fighter_component, ai=ai_component)
            elif choice == 'orc-warlord':

                fighter_component = Fighter(hp=45, defense=2, power=4, xp=150, death_function=monster_death)
                ai_component = BasicMonster()

                monster = Object(x, y, 'o', 'Orc', libtcod.green,
                                 blocks=True, fighter=fighter_component, ai=ai_component)

            elif choice == 'goblin_group':
                equipment_component = Equipment(slot='right hand', power_bonus=2)
                dagger = Object(0, 0, '-', 'dagger', libtcod.sky,item = Item(count=1,use_function=None,countable = False,who_owns = player.fighter),equipment=equipment_component)
                fighter_component = Fighter(hp=35, defense=0, power=6, xp=125, death_function=monster_death)
                fighter_component.inventory.append(dagger)
                ai_component = BasicMonster()

                monster = Object(x, y, 'g', 'goblin leader', libtcod.red,
                                 blocks=True, fighter=fighter_component, ai=ai_component)

                fighter_component = Fighter(hp=20, defense=0, power=4, xp=35,speed=75, death_function=monster_death)
                ai_component = BasicMonster()
                goblin_minor =  Object(x, y, 'g', 'goblin', libtcod.dark_pink,
                                 blocks=True, fighter=fighter_component, ai=ai_component)
                print(create_random())
                some_objects_spawn(x,y, goblin_minor,3)

            objects.append(monster)

    #choose random number of items
    num_items = libtcod.random_get_int(0, 0, max_items)

    for i in range(num_items):
        #choose random spot for this item
        x = libtcod.random_get_int(0, room.x1+1, room.x2-1)
        y = libtcod.random_get_int(0, room.y1+1, room.y2-1)

        #only place it if the tile is not blocked
        if not is_blocked(x, y):

            choice = random_choice(item_chances)

            if choice == 'wand of flame':
                #create a healing potion
                flame =cast_damage_spell(damage=5,char='&',dur=1,effectname='fire',effect = 'burn up',rad=1,mana=0,color=libtcod.dark_orange)
                item_component= Item(1,use_function=flame.casting)
                wand_component = wand(5,5)
                item = Object(x, y, '/', 'wand of flame', libtcod.darker_gray,item = item_component,wand=wand_component)

            elif choice == 'heal':
                #create a healing potion
                item_component = Item(1,use_function=cast_heal,countable = True)
                item = Object(x, y, '!', 'healing potion', libtcod.violet, item=item_component)

            elif choice == 'bow':
                #create a bow
                ranged_component = ranged(ammo_type='arrows',who_owns= None)
                equipment_component = Equipment(slot='right hand',power_bonus=2,ranged=ranged_component)
                item_component= Item(1,use_function = None,countable = False)
                item = Object(x, y, ')', 'bow', libtcod.violet, equipment=equipment_component,item=item_component)

            elif choice == 'lightning':
                #create a lightning bolt scroll
                lightning = cast_damage_spell(damage=10,char='&',dur=1, effect ='lights' ,effectname='lightning',rad=1,mana=0,color=libtcod.light_cyan)
                item_component = Item(1,use_function=lightning.casting,countable = True)
                item = Object(x, y, '#', 'scroll of lightning bolt', libtcod.light_yellow, item=item_component)

            elif choice == 'arrow':
                #create a lightning bolt scroll
                random = libtcod.random_get_int(0,5,25)
                item_component = Item(1,use_function = None,countable = True)
                item_component.count=random
                item = Object(x, y, ';', 'arrows', libtcod.light_yellow, item=item_component)

            elif choice == 'fireball':
                #create a fireball scroll
                fireball = cast_damage_spell(damage=10,char='^',dur=1, effect ='burns' ,effectname='fire',rad=2,mana=0,color=libtcod.orange)
                item_component = Item(1,use_function=fireball.casting,countable = True)
                item = Object(x, y, '#', 'scroll of fireball', libtcod.light_yellow, item=item_component)

            elif choice == 'confuse':
                #create a confuse scroll
                item_component = Item(1,use_function=cast_confuse,countable = True)
                item = Object(x, y, '#', 'scroll of confusion', libtcod.light_yellow, item=item_component)

            elif choice == 'sword':
                #create a sword
                equipment_component = Equipment(slot='right hand', power_bonus=3)
                item = Object(x, y, '/', 'sword', libtcod.sky, equipment=equipment_component)

            elif choice == 'shield':
                #create a shield
                equipment_component = Equipment(slot='left hand', defense_bonus=1)
                item = Object(x, y, '[', 'shield', libtcod.darker_orange, equipment=equipment_component)

            elif choice == 'boots':
                #create a shield
                equipment_component = Equipment(slot='legs', defense_bonus=1)
                item = Object(x, y, ':', 'boots', libtcod.darker_orange, equipment=equipment_component)

            elif choice == 'cap':
                #create a shield
                equipment_component = Equipment(slot='head', defense_bonus=1)
                item = Object(x, y, '<', 'cap', libtcod.darker_orange, equipment=equipment_component)

            elif choice == 'leather armor':
                #create a shield
                equipment_component = Equipment(slot='torso', defense_bonus=1)
                item = Object(x, y, 'V', 'Leather Armor', libtcod.darker_orange, equipment=equipment_component)

            objects.append(item)
            #items appear below other objects
            item.always_visible = True  #items are visible even out-of-FOV, if in an explored area

def addeffect(x,y,duration,char,name,color):
    effect_component = Effect(duration)
    effect = Object(x,y,char,name,color,effect = effect_component)
    effects.append(effect)

def render_bar(x, y, total_width, name, value, maximum, bar_color, back_color):
    #render a bar (HP, experience, etc). first calculate the width of the bar
    bar_width = int(float(value) / maximum * total_width)

    #render the background first
    libtcod.console_set_default_background(panel, back_color)
    libtcod.console_rect(panel, x, y, total_width, 1, False, libtcod.BKGND_SCREEN)

    #now render the bar on top
    libtcod.console_set_default_background(panel, bar_color)
    if bar_width > 0:
        libtcod.console_rect(panel, x, y, bar_width, 1, False, libtcod.BKGND_SCREEN)

    #finally, some centered text with the values
    libtcod.console_set_default_foreground(panel, libtcod.white)
    libtcod.console_print_ex(panel, x + total_width / 2, y, libtcod.BKGND_NONE, libtcod.CENTER,
                                 name + ': ' + str(value) + '/' + str(maximum))

def get_names_under_mouse():
    global mouse
    #return a string with the names of all objects under the mouse

    (x, y) = (mouse.cx, mouse.cy)
    names=[]

    #create a list with the names and stacks of all objects at the mouse's coordinates and in FOV
    for obj in objects:
        if obj.x==x and obj.y==y and libtcod.map_is_in_fov(fov_map,obj.x,obj.y):
            if obj.item and obj.item.countable:names.append(obj.name+' '+str(obj.item.count))
            else:names.append(obj.name)

    #Add effects name to the list!
    names.extend([obj.name for obj in effects
             if obj.x == x and obj.y == y and libtcod.map_is_in_fov(fov_map,obj.x,obj.y)])
    names = ', '.join(names)  #join the names, separated by commas
    return names.capitalize()

def render_all():
    global fov_map, color_dark_wall, color_light_wall
    global color_dark_ground, color_light_ground
    global fov_recompute

    if fov_recompute:
        #recompute FOV if needed (the player moved or something)
        fov_recompute = False
        libtcod.map_compute_fov(fov_map, player.x, player.y, TORCH_RADIUS, FOV_LIGHT_WALLS, FOV_ALGO)

        #go through all tiles, and set their background color according to the FOV
        for y in range(MAP_HEIGHT):
            for x in range(MAP_WIDTH):
                visible = libtcod.map_is_in_fov(fov_map, x, y)
                wall = map[x][y].block_sight
                if not visible:
                    #if it's not visible right now, the player can only see it if it's explored
                    if map[x][y].explored:
                        if wall:
                            libtcod.console_set_char_background(con, x, y, color_dark_wall, libtcod.BKGND_SET)
                        else:
                            libtcod.console_set_char_background(con, x, y, color_dark_ground, libtcod.BKGND_SET)
                else:
                    #it's visible
                    if wall:
                        libtcod.console_set_char_background(con, x, y, color_light_wall, libtcod.BKGND_SET )
                    else:
                        libtcod.console_set_char_background(con, x, y, color_light_ground, libtcod.BKGND_SET )
                        #since it's visible, explore it
                    map[x][y].explored = True

    #draw all objects in the list, except the player. we want it to
    #always appear over all other objects! so it's drawn later.

    for object in objects:
        if not object.fighter:
            object.draw()

    for object in objects:
        if object.fighter:
            object.draw()


    player.draw()
    for effect in effects:
        effect.draw()
    #blit the contents of "con" to the root console
    libtcod.console_blit(con, 0, 0, MAP_WIDTH, MAP_HEIGHT, 0, 0, 0)


    #prepare to render the GUI panel
    libtcod.console_set_default_background(panel, libtcod.black)
    libtcod.console_clear(panel)

    #print the game messages, one line at a time
    y = 1
    for (line, color) in game_msgs:
        libtcod.console_set_default_foreground(panel, color)
        libtcod.console_print_ex(panel, MSG_X, y, libtcod.BKGND_NONE, libtcod.LEFT,line)
        y += 1

    #show the player's stats
    render_bar(1, 1, BAR_WIDTH, 'HP', player.fighter.hp, player.fighter.max_hp,
               libtcod.light_red, libtcod.darker_red)
    libtcod.console_print_ex(panel, 1, 3, libtcod.BKGND_NONE, libtcod.LEFT, 'Dungeon level ' + str(dungeon_level))

    #display names of objects under the mouse
    libtcod.console_set_default_foreground(panel, libtcod.light_gray)
    libtcod.console_print_ex(panel, 1, 0, libtcod.BKGND_NONE, libtcod.LEFT, get_names_under_mouse())

    #blit the contents of "panel" to the root console
    libtcod.console_blit(panel, 0, 0, SCREEN_WIDTH, PANEL_HEIGHT, 0, 0, PANEL_Y)


def message(new_msg, color = libtcod.white):
    #split the message if necessary, among multiple lines
    new_msg_lines = textwrap.wrap(new_msg, MSG_WIDTH)

    for line in new_msg_lines:
        #if the buffer is full, remove the first line to make room for the new one
        if len(game_msgs) == MSG_HEIGHT:
            del game_msgs[0]

        #add the new line as a tuple, with the text and the color
        game_msgs.append( (line, color) )


def player_move_or_attack(dx, dy):
    global fov_recompute

    #the coordinates the player is moving to/attacking
    x = player.x + dx
    y = player.y + dy

    #try to find an attackable object there
    target = None
    for object in objects:
        if object.fighter and object.x == x and object.y == y:
            target = object
            break

    #attack if target found, move otherwise
    if target is not None:
        player.fighter.attack(target)
    else:
        player.move(dx, dy)
        fov_recompute = True
    #By default one move takes 100 speedpoints
    player.fighter.speed-=100

def menu(header, options, width):
    if len(options) > 26: raise ValueError('Cannot have a menu with more than 26 options.')

    #calculate total height for the header (after auto-wrap) and one line per option
    header_height = libtcod.console_get_height_rect(con, 0, 0, width, SCREEN_HEIGHT, header)
    if header == '':
        header_height = 0
    height = len(options) + header_height

    #create an off-screen console that represents the menu's window
    window = libtcod.console_new(width, height)

    #print the header, with auto-wrap
    libtcod.console_set_default_foreground(window, libtcod.white)
    libtcod.console_print_rect_ex(window, 0, 0, width, height, libtcod.BKGND_NONE, libtcod.LEFT, header)

    #print all the options
    y = header_height
    letter_index = ord('a')
    for option_text in options:
        text = '(' + chr(letter_index) + ') ' + option_text
        libtcod.console_print_ex(window, 0, y, libtcod.BKGND_NONE, libtcod.LEFT, text)
        y += 1
        letter_index += 1

    #blit the contents of "window" to the root console
    x = SCREEN_WIDTH/2 - width/2
    y = SCREEN_HEIGHT/2 - height/2
    libtcod.console_blit(window, 0, 0, width, height, 0, x, y, 1.0, 0.7)

    #present the root console to the player and wait for a key-press
    libtcod.console_flush()
    key = libtcod.console_wait_for_keypress(True)

    if key.vk == libtcod.KEY_ENTER and key.lalt:  #(special case) Alt+Enter: toggle fullscreen
        libtcod.console_set_fullscreen(not libtcod.console_is_fullscreen)

    #convert the ASCII code to an index; if it corresponds to an option, return it
    index = key.c - ord('a')
    if index >= 0 and index < len(options): return index
    return None

def inventory_menu(header):
    #show a menu with each item of the inventory as an option
    inventory=player.fighter.inventory
    if len(inventory) == 0:
        options = ['Inventory is empty.']
    else:
        options = []
        for item in player.fighter.inventory:
            text = item.name
            equipment = item.equipment
            countable = item.item.countable
            wand = item.wand
            #show additional information, in case it's equipped
            if item.item.count>1 and countable:
                text = text +' in count ' +str(item.item.count)
            if equipment and item.equipment.is_equipped:
                text = text + ' (on ' + item.equipment.slot + ')'
            if wand:
                text = text + ' '+str(item.wand.charges)+" \ "+str(item.wand.max_charges)
            options.append(text)

    index = menu(header, options, INVENTORY_WIDTH)

    #if an item was chosen, return it
    if index is None or len(inventory) == 0: return None
    return inventory[index].item

def msgbox(text, width=50):
    menu(text, [], width)  #use menu() as a sort of "message box"

def handle_keys():
    global key

    if key.vk == libtcod.KEY_ENTER and key.lalt:
        #Alt+Enter: toggle fullscreen
        libtcod.console_set_fullscreen(not libtcod.console_is_fullscreen())

    elif key.vk == libtcod.KEY_ESCAPE:
        return 'exit'  #exit game

    if game_state == 'playing':
        #movement keys
      if key.vk!=None:
        if key.vk == libtcod.KEY_UP or key.vk == libtcod.KEY_KP8:
            player_move_or_attack(0, -1)

        elif key.vk == libtcod.KEY_DOWN or key.vk == libtcod.KEY_KP2:
            player_move_or_attack(0, 1)
        elif key.vk == libtcod.KEY_LEFT or key.vk == libtcod.KEY_KP4:
            player_move_or_attack(-1, 0)
        elif key.vk == libtcod.KEY_RIGHT or key.vk == libtcod.KEY_KP6:
            player_move_or_attack(1, 0)
        elif key.vk == libtcod.KEY_HOME or key.vk == libtcod.KEY_KP7:
            player_move_or_attack(-1, -1)
        elif key.vk == libtcod.KEY_PAGEUP or key.vk == libtcod.KEY_KP9:
            player_move_or_attack(1, -1)
        elif key.vk == libtcod.KEY_END or key.vk == libtcod.KEY_KP1:
            player_move_or_attack(-1, 1)
        elif key.vk == libtcod.KEY_PAGEDOWN or key.vk == libtcod.KEY_KP3:
            player_move_or_attack(1, 1)
        elif key.vk == libtcod.KEY_KP5:
            pass  #do nothing ie wait for the monster to come to you
        else:
            #test for other keys
            key_char = chr(key.c)
            if key_char == 'g':
                #pick up an item
                for object in objects:  #look for an item in the player's tile
                    if object.x == player.x and object.y == player.y and object.item:
                        object.item.pick_up()
                        break
            if key_char == 'f':
               weapon = get_equipped_in_slot('right hand',player.fighter)
               if weapon:
                 if weapon.equipment.ranged:
                    weapon.equipment.ranged.fire()
            if key_char == 'i':
                #show the inventory; if an item is selected, use it
                chosen_item = inventory_menu('Press the key next to an item to use it, or any other to cancel.\n')
                if chosen_item is not None:
                    chosen_item.use()
            if key_char == 'd':
                #show the inventory; if an item is selected, drop it
                chosen_item = inventory_menu('Press the key next to an item to drop it, or any other to cancel.\n')
                if chosen_item is not None:
                    chosen_item.drop()

            if key_char == 'c':
                #show character information
                level_up_xp = LEVEL_UP_BASE + player.level * LEVEL_UP_FACTOR
                msgbox('Character Information\n\nLevel: ' + str(player.level) + '\nExperience: ' + str(player.fighter.xp) +
                       '\nExperience to level up: ' + str(level_up_xp) + '\n\nMaximum HP: ' + str(player.fighter.max_hp) +
                       '\nAttack: ' + str(player.fighter.power) + '\nDefense: ' + str(player.fighter.defense), CHARACTER_SCREEN_WIDTH)

            if key_char == '<':
                #go down stairs, if the player is on them
                if stairs.x == player.x and stairs.y == player.y:
                    next_level()
                return 'didnt-take-turn'

def check_level_up():
    #see if the player's experience is enough to level-up
    level_up_xp = LEVEL_UP_BASE + player.level * LEVEL_UP_FACTOR
    if player.fighter.xp >= level_up_xp:
        #it is! level up and ask to raise some stats
        player.level += 1
        player.fighter.xp -= level_up_xp
        message('Your battle skills grow stronger! You reached level ' + str(player.level) + '!', libtcod.yellow)

        choice = None
        while choice == None:  #keep asking until a choice is made
            choice = menu('Level up! Choose a stat to raise:\n',
                          ['Constitution (+20 HP, from ' + str(player.fighter.max_hp) + ')',
                           'Strength (+1 attack, from ' + str(player.fighter.power) + ')',
                           'Agility (+1 defense, from ' + str(player.fighter.defense) + ')'], LEVEL_SCREEN_WIDTH)

        if choice == 0:
            player.fighter.base_max_hp += 20
            player.fighter.hp += 20
        elif choice == 1:
            player.fighter.base_power += 1
        elif choice == 2:
            player.fighter.base_defense += 1

def player_death(player):
    #the game ended!
    global game_state
    message('You died!', libtcod.red)
    game_state = 'dead'

    #for added effect, transform the player into a corpse!
    player.char = '%'
    player.color = libtcod.dark_red

def monster_death(monster):
    #transform it into a nasty corpse! it doesn't block, can't be
    #attacked and doesn't move
    message('The ' + monster.name + ' is dead! You gain ' + str(monster.fighter.xp) + ' experience points.', libtcod.orange)
    print(monster.name)
    print('YOOOOO')
    print(monster.fighter.inventory)
    for i in monster.fighter.inventory:
        print(i.name)
        if i.equipment:
            i.equipment.dequip()
        i.who_owns = None
        item = i
        item.x = monster.x
        item.y = monster.y
        item.who_owns=None
        monster.fighter.inventory.remove(i)
        if sort_tile(item.x,item.y,item) == False:
         i.who_owns=None
         objects.append(item)
    monster.char = '%'
    monster.color = libtcod.dark_red
    monster.blocks = False
    monster.fighter.inventory=None
    monster.fighter = None
    monster.ai = None
    monster.name = 'remains of ' + monster.name
    monster.send_to_back()

def target_tile(max_range=None):
    global key, mouse
    #return the position of a tile left-clicked in player's FOV (optionally in a range), or (None,None) if right-clicked.
    while True:
        #render the screen. this erases the inventory and shows the names of objects under the mouse.
        libtcod.console_flush()
        libtcod.sys_check_for_event(libtcod.EVENT_KEY_PRESS | libtcod.EVENT_MOUSE, key, mouse)
        render_all()

        (x, y) = (mouse.cx, mouse.cy)
        print(str(x),str(y)+'Yooo'+str(player.x),str(player.y))
        if mouse.rbutton_pressed or key.vk == libtcod.KEY_ESCAPE:
            return (None, None)  #cancel if the player right-clicked or pressed Escape

        #accept the target if the player clicked in FOV, and in case a range is specified, if it's in that range
        if (mouse.lbutton_pressed and libtcod.map_is_in_fov(fov_map, x, y) and
                (max_range is None or player.distance(x, y) <= max_range)):
                 return (x, y)

def target_monster(max_range=None):
    #returns a clicked monster inside FOV up to a range, or None if right-clicked
    while True:
        (x, y) = target_tile(max_range)
        if x is None:  #player cancelled
            return None

        #return the first clicked monster, otherwise continue looping
        for obj in objects:
            if obj.x == x and obj.y == y and obj.fighter and obj != player:
                return obj

def closest_monster(max_range):
    #find closest enemy, up to a maximum range, and in the player's FOV
    closest_enemy = None
    closest_dist = max_range + 1  #start with (slightly more than) maximum range

    for object in objects:
        if object.fighter and not object == player and libtcod.map_is_in_fov(fov_map, object.x, object.y):
            #calculate distance between this object and the player
            dist = player.distance_to(object)
            if dist < closest_dist:  #it's closer, so remember it
                closest_enemy = object
                closest_dist = dist
    return closest_enemy

def cast_heal(who):
    if who==None:
        who=player
    #heal the player
    if who.hp == who.max_hp:
        if who==Player:
           message('You are already at full health.', libtcod.red)
           return 'cancelled'
    if who==player:
        message('Your wounds start to feel better!', libtcod.light_violet)
    who.heal(HEAL_AMOUNT)

def cast_fireball():
    #ask the player for a target tile to throw a fireball at
    message('Left-click a target tile for the fireball, or right-click to cancel.', libtcod.light_cyan)
    (x, y) = target_tile()
    if x is None: return 'cancelled'
    message('The fireball explodes, burning everything within ' + str(FIREBALL_RADIUS) + ' tiles!', libtcod.orange)
    #create fire effect for every tile in zoner=

    for z in range(x-FIREBALL_RADIUS, x+FIREBALL_RADIUS):
        for i in range(y-FIREBALL_RADIUS,y+FIREBALL_RADIUS):
         wall = map[z][i].block_sight
         if not wall:
             addeffect(z,i,2,'^','Fire',libtcod.orange)
    for obj in objects:  #damage every fighter in range, including the player
        if obj.distance(x, y) <= FIREBALL_RADIUS and obj.fighter:
            message('The ' + obj.name + ' gets burned for ' + str(FIREBALL_DAMAGE) + ' hit points.', libtcod.orange)
            obj.fighter.take_damage(FIREBALL_DAMAGE)

def cast_confuse(who):
    #ask the player for a target to confuse
    if who==player.fighter:
        message('Left-click an enemy to confuse it, or right-click to cancel.', libtcod.light_cyan)
        monster = target_monster(CONFUSE_RANGE)
    else:
        monster = player.fighter
    if monster is None: return 'cancelled'

    #replace the monster's AI with a "confused" one; a    fter some turns it will restore the old AI
    old_ai = monster.ai
    monster.ai = ConfusedMonster(old_ai)
    monster.ai.owner = monster  #tell the new component who owns it
    message('The eyes of the ' + monster.name + ' look vacant, as he starts to stumble around!', libtcod.light_green)
    addeffect(monster.x,monster.y,3,'%','Smoke',libtcod.gray)

class cast_damage_spell():

    def __init__(self,damage,char,dur,effectname,effect= 'Damaging',rad=1,mana=0,color = libtcod.white):
        self.damage = damage
        self.char = char
        self.dur = dur
        self.rad = rad
        self.mana = mana
        self.effectname = effectname
        self.color = color
        self.effect = effect

    def casting(self,who):

        message('Left-click a target tile for the flame, or right-click to cancel.', libtcod.light_cyan)
        if who==player.fighter:
           print('Casting from player~!')
           (x, y) = target_tile()
           print(str(mouse.cx),str(mouse.cy)+'MOUSE CORDS')
        else:
            x=player.x
            y=player.y
        if x is None: return 'cancelled'

        message(self.effectname+' '+ self.effect + ' a tile!')
        print(str(who.owner.x))
        print(str(who.owner.y))
        linefunc = line(who.owner.x,who.owner.y,x,y)
        #If it's succesfully computed, add flame effect at tile, and if there are any monster over there, burn him!

        if linefunc.compute() == True:
        # IF computed, on the targeted tile.

           for z in range(linefunc.stuck_x-self.rad, linefunc.stuck_x+self.rad):
              for i in range(linefunc.stuck_y-self.rad,linefunc.stuck_y+self.rad):
                  wall = map[z][i].block_sight
                  if not wall:
                       addeffect(z,i,duration=self.dur,char=self.char, name=self.effectname,color=self.color)
                       for obj in objects:
                        if obj.x==z and obj.y==i and obj.fighter:
                             message('The '+obj.name + ' damaged for ' + str(self.damage)+ 'hit points.',libtcod.orange)
                             obj.fighter.take_damage(self.damage)

def save_game():
    #open a new empty shelve (possibly overwriting an old one) to write the game data
    file = shelve.open('savegame', 'n')
    file['map'] = map
    file['objects'] = objects
    file['player_index'] = objects.index(player)  #index of player in objects list
    file['stairs_index'] = objects.index(stairs)  #same for the stairs
    file['inventory'] = inventory
    file['game_msgs'] = game_msgs
    file['game_state'] = game_state
    file['dungeon_level'] = dungeon_level
    file.close()

def load_game():
    #open the previously saved shelve and load the game data
    global map, objects, player, stairs, inventory, game_msgs, game_state, dungeon_level,effects

    file = shelve.open('savegame', 'r')
    map = file['map']
    objects = file['objects']
    player = objects[file['player_index']]  #get index of player in objects list and access it
    stairs = objects[file['stairs_index']]  #same for the stairs
    inventory = file['inventory']
    game_msgs = file['game_msgs']
    game_state = file['game_state']
    dungeon_level = file['dungeon_level']
    effects = file['effects']
    file.close()

    initialize_fov()

class line:
    def __init__(self,x1,y1,x2,y2,stuck_x=None,stuck_y=None):
     self.x1 = x1
     self.x2 = x2
     self.y2 = y2
     self.y1 = y1
     self.stuck_x = stuck_x
     self.stuck_y = stuck_y

    def compute_towards(self):
      dx=self.x2-self.x1
      dy=self.y2-self.y1
      errorX = 0
      errorY = 0
      if dx > 0:
         incX= 1
      elif dx == 0:
         incX= 0
      else:
         incX = -1
      if dy > 0:
        incY= 1
      elif dy ==0:
        incY = 0
      else:
        incY = -1
      dx = abs(dx)
      dy = abs(dy)
      if dy > dx:
         d = dy
      else:
         d = dx
      currentX = self.x1
      currentY = self.y1

      while currentX <> self.x2 or (currentY <> self.y2):
        errorX= errorX + dx
        errorY= errorY + dy
        if errorX >= d:
           errorX = errorX - d
           block = map[currentX + incX][currentY].block_sight
           if block:
               return False
           else:
               currentX= currentX + incX

        if errorY >= d:
          errorY= errorY - d
          block = map[currentX][currentY + incY].block_sight
          if block:
              return False
          else:
               currentY= currentY + incY;
        if self.x2==currentX and self.y2== currentY:
            return True

        for obje in objects:
            if obje.x == currentX and obje.y == currentY and obje.blocks and obje.x<>self.x1 and obje.y<>self.y1:
                return False


    def compute(self):

      print(self.x2,self.y2,self.x1,self.y1)
      dx=self.x2-self.x1
      dy=self.y2-self.y1
      errorX = 0
      errorY = 0
      if dx > 0:
         incX= 1
      elif dx == 0:
         incX= 0
      else:
         incX = -1
      if dy > 0:
        incY= 1
      elif dy ==0:
        incY = 0
      else:
        incY = -1
      dx = abs(dx)
      dy = abs(dy)
      if dy > dx:
         d = dy
      else:
         d = dx
      currentX = self.x1
      currentY = self.y1
      while currentX != self.x2 or currentY != self.y2:
        print(currentX,currentY)
        errorX= errorX + dx
        errorY= errorY + dy
        if errorX >= d:
           errorX = errorX - d
           block = map[currentX + incX][currentY].block_sight
           if block:
               self.stuck_x = currentX
               self.stuck_y = currentY
               return True
           else:
               currentX= currentX + incX
        if errorY >= d:
          errorY= errorY - d
          block = map[currentX][currentY + incY].block_sight
          if block:
              self.stuck_x = currentX
              self.stuck_y = currentY
              return True
          else:
               currentY= currentY + incY;

        for obje in objects:
            if obje.x == currentX and obje.y == currentY and obje.blocks :
                self.stuck_x = currentX
                self.stuck_y = currentY
                return True
      self.stuck_x = currentX
      self.stuck_y = currentY
      return True

def new_game():
    global player, inventory, game_msgs, game_state, dungeon_level, player_race,player_background

    #create object representing the player
    #fighter_component = Fighter(hp=100, defense=1, power=2, xp=0, death_function=player_death,hungry = 100)
    player = attribute_selection_player(sex_Male,player_race,player_background)

    player.level = 1

    #generate map (at this point it's not drawn to the screen)
    dungeon_level = 1
    make_entrance_map()
    initialize_fov()

    game_state = 'playing'
    inventory = []
    effects = []
    #create the list of game messages and their colors, starts empty
    game_msgs = []

    #a warm welcoming message!
    message(player.name+', go to the deeps of this castle, and find treasures!''.', libtcod.red)
    print(player.name)
    #initial equipment: a dagger
    lightning = cast_damage_spell(damage=10,char='&',dur=1, effect ='lights' ,effectname='lightning',rad=1,mana=0,color=libtcod.light_cyan)
    item_component = Item(1,use_function=lightning.casting,countable = True, who_owns=player.fighter)
    scroll = Object(0, 0, '#', 'scroll of lightning bolt', libtcod.light_yellow, item=item_component)

    item_component = Item(1,use_function=cast_confuse,countable = True,who_owns=player.fighter)
    confuse = Object(0, 0, '#', 'scroll of confusion', libtcod.light_yellow, item=item_component)
    equipment_component = Equipment(slot='right hand', power_bonus=5)
    dag = Object(0, 0, '-', 'dagger', libtcod.sky,item = Item(count=1,use_function=None,countable = False,who_owns = player.fighter),equipment=equipment_component)
    print(player.name)
    dag.item.who_owns=player.fighter
    print(dag.item.who_owns)
    player.fighter.inventory.append(dag)
    player.fighter.inventory.append(scroll)
    player.fighter.inventory.append(confuse)
    equipment_component.equip()
    dag.always_visible = True

def next_level():
    #advance to the next level
    global dungeon_level
    message('You take a moment to rest, and recover your strength.', libtcod.light_violet)
    player.fighter.heal(player.fighter.max_hp / 2)  #heal the player by 50%

    dungeon_level += 1
    message('After a rare moment of peace, you descend deeper into the heart of castle...', libtcod.red)
    make_map()  #create a fresh new level!
    initialize_fov()

def initialize_fov():
    global fov_recompute, fov_map
    fov_recompute = True

    #create the FOV map, according to the generated map
    fov_map = libtcod.map_new(MAP_WIDTH, MAP_HEIGHT)
    for y in range(MAP_HEIGHT):
        for x in range(MAP_WIDTH):
            libtcod.map_set_properties(fov_map, x, y, not map[x][y].block_sight, not map[x][y].blocked)

    libtcod.console_clear(con)  #unexplored areas start black (which is the default background color)
############################################################################################################################################################
########################
############################################################################################################################################################
#########################################################################################################################################################################
class Proffesion():

    def __init__(self,name,ai,boosts=[],unboosts=[]):
        self.name = name
        self.ai = ai
        self.boosts=boosts
        self.unboosts=[]
#Name of the race, it's attributes, stats, etc

class Race():

    def __init__(self,name,chars=[],color=libtcod.red,sexs=[],abillities=[],stats=[None,None,None,None],proffesions=[]):
        self.name = name
        self.sexs = sexs
        self.abillities = abillities
        self.stats = stats
        self.chars=chars
        self.color=color
        self.proffesions=proffesions


#The stats dependends on sex of the mo
class Sex():

    def __init__(self,name,negative_attitude=[],positive_attitude=[]):
        self.name = name
        self.negative_attitude = negative_attitude
        self.positive_attitude = positive_attitude




leader_job = Proffesion('Leader',CleverMonster,['hp'],['speed'])
warrior_job = Proffesion('warrior',CleverMonster,['hp','power'],['speed'])
archer_job = Proffesion('archer',RangerMonster,['defence','speed'])
worker_job  =  Proffesion('worker',CleverMonster,['power'],['speed','hp'])

sex_Female = Sex('Female',['power','hp'])
sex_Male = Sex('Male',[],['power'])


race_Humans = Race(name='Human ',chars=['@','H','h','t'],sexs=[sex_Female,sex_Male],stats=[30,1,5,100],color=[libtcod.lighter_yellow,libtcod.darker_gray,libtcod.darker_yellow,libtcod.darker_amber],proffesions=[leader_job,warrior_job,worker_job,archer_job])
print(race_Humans.proffesions)
race_Goblins = Race(name='Goblin ',chars=['g','G','I'],sexs=[sex_Female,sex_Male],stats=[20,3,3,75],color=[libtcod.red,libtcod.darker_red,libtcod.darker_crimson,libtcod.darker_green],proffesions=[leader_job,warrior_job,archer_job])
races = [race_Humans,race_Goblins]


def create_random():
    #Base of new character
    print(races[0].name)
    race = races[libtcod.random_get_int(0,0,len(races)-1)]
    print(race.name)
    sex = race.sexs[libtcod.random_get_int(0,0,len(race.sexs)-1)]

    proffession = race.proffesions[libtcod.random_get_int(0,0,len(race.proffesions)-1)]
    return attribute_selection(sex,race,proffession)

def attribute_selection_player(sex,race,proffession):
    speed = race.stats[3]
    hp=race.stats[0]
    hp=100
    power=race.stats[2]
    defence=race.stats[1]
    for i in proffession.boosts:
        if i=='power':
            power+2
        elif i=='hp':
            hp+50
        elif i=='defenece':
            defence+2
    for i in sex.positive_attitude:
        if i=='power':
            power+2
        elif i=='hp':
            hp+20
        elif i=='defence':
            defence+2
    for i in sex.negative_attitude:
        if i=='power':
            power-2
        elif i=='hp':
            hp-10
        elif i=='defence':
            defence-1
    if power<0:
        power=1
    if hp<5:
        hp=5
    if defence<0:
        defence=1
    ai_component = proffession.ai()
    fighter_component = Fighter(hp=hp, defense=defence, power=power, xp=100, death_function= player_death,inventory=[])
    player = Object(x=0,y=0,char='@', name=sex.name+' '+race.name+' '+proffession.name, color=libtcod.white, blocks=True, fighter=fighter_component)
    return player

def attribute_selection(sex,race,proffession):
    print(1+1)
    hp=race.stats[0]
    power=race.stats[2]
    defence=race.stats[1]
    speed = race.stats[3]

    for i in proffession.boosts:
        if i=='power':
            print(str(power)+'do')
            power+=2
            power+=libtcod.random_get_int(0,0,dungeon_level)
            print(str(power)+'posle')
        elif i=='hp':
            hp+10
            hp+=libtcod.random_get_int(0,5*dungeon_level,10*dungeon_level)
        elif i=='defenece':
            defence+=2
            defence+=libtcod.random_get_int(0,0,dungeon_level)
        elif i=='speed':
            speed+=35
            speed+=libtcod.random_get_int(0,0,dungeon_level)
    for i in proffession.unboosts:
        if i=='power':
            power-=2
        elif i=='hp':
            hp-=50
        elif i=='defenece':
            defence-=2
        elif i=='speed':
            speed-=35
            defence+=libtcod.random_get_int(0,0,dungeon_level)
    for i in sex.positive_attitude:
        if i=='power':
            power+=2
            power+=libtcod.random_get_int(0,0,dungeon_level)
        elif i=='hp':
            hp+=10
        elif i=='defence':
            defence+=2
            defence+=libtcod.random_get_int(0,0,dungeon_level)
        elif i=='speed':
            speed+=35
            speed+=libtcod.random_get_int(0,0,dungeon_level)
    for i in sex.negative_attitude:
        if i=='power':
            power-2
        elif i=='hp':
            hp-10
        elif i=='defence':
            defence-1
        elif i=='speed':
            speed+=35
            speed+=libtcod.random_get_int(0,0,dungeon_level)

    if power<0:
        power=1
    if hp<5:
        hp=5
    if defence<0:
        defence=1
    ai_component = proffession.ai()
    fighter_component = Fighter(hp=hp, defense=defence, power=power, xp=100, death_function=monster_death,inventory=[])
    fighter = Object(x=0,y=0,char=race.chars[libtcod.random_get_int(0,0,len(race.chars)-1)], name=sex.name+' '+race.name+' '+proffession.name, color=random.choice(race.color), blocks=True, fighter=fighter_component, ai=ai_component )
    print(power)
    return fighter
#########################################################################################################################################################################





##############################################################################################################################################################################################################################################################################################






#####################################################################################################################
def play_game():
    global key, mouse

    player_action = None

    mouse = libtcod.Mouse()
    key = libtcod.Key()
    #main loop
    while not libtcod.console_is_window_closed():
        libtcod.sys_check_for_event(libtcod.EVENT_KEY_PRESS | libtcod.EVENT_MOUSE, key, mouse)
        #render the screen
        render_all()

        libtcod.console_flush()

        #level up if needed
        check_level_up()

        #erase all objects at their old locations, before they move

        for effect in effects:
            effect.clear()
        for object in objects:
            object.clear()
        #handle keys and exit game if needed
        player_action = handle_keys()
        if player_action == 'exit':
            save_game()
            break

        #let monsters take their turn
        if game_state == 'playing' and player.fighter.speed<0:
                player.fighter.speed+=player.fighter.default_speed
                ai_turn()
                render_all()
        else:
                render_all()
def ai_turn():
    #Not like other things, if player speed value lower, than player speed higher! DONT FORGET YOU
       for effect in effects:

                effect.effect.continues()
       for object in objects:

         if object.fighter and object.ai:

             if object.fighter.speed<=0:

                object.fighter.speed+=object.fighter.default_speed

             while object.fighter.speed>0:
                 #Default action point's to go or smthing
                  if object.fighter.speed<100:

                      object.fighter.speed+=object.fighter.default_speed
                      break

                  object.fighter.every_turn()
                  object.ai.take_turn()
def background_choose():
    libtcod.console_set_default_foreground(0, libtcod.light_red)
    key = libtcod.console_wait_for_keypress(True)
    background_choice = menu('', ['Warrior', 'Archer', 'No','No'], 22)
    if  background_choice == 0:
        player_background = warrior_job
    if  background_choice == 1:
        player_background = archer_job
    if  background_choice == 2:
        player_race =race_Orcs
    if  background_choice == 3:
       player_race =race_Elfs
    new_game()
    play_game()
    global player_background
def race_choose():
    libtcod.console_set_default_foreground(0, libtcod.light_red)
    key = libtcod.console_wait_for_keypress(True)
    race_choice = menu('', ['Human', 'Goblin', 'Orc','Elf'], 22)
    if race_choice == 0:
        player_race =race_Humans
    if race_choice == 1:
        player_race =race_Goblins
    if race_choice == 2:
        player_race =race_Orcs
    if race_choice == 3:
       player_race =race_Elfs
    background_choose()
    global player_race
def main_menu():
    img = libtcod.image_load('menu_background.png')

    while not libtcod.console_is_window_closed():
        #show the background image, at twice the regular console resolution
        libtcod.image_blit_2x(img, 0, 0, 0)

        #show the game's title, and some credits!
        libtcod.console_set_default_foreground(0, libtcod.light_red)
        libtcod.console_print_ex(0, SCREEN_WIDTH/2, SCREEN_HEIGHT/6-4, libtcod.BKGND_NONE, libtcod.CENTER,
                                 'CAVES OF THE DEATH')
        libtcod.console_print_ex(0, SCREEN_WIDTH/2, SCREEN_HEIGHT-2, libtcod.BKGND_NONE, libtcod.CENTER, 'By Jummixe')

        #show options and wait for the player's choice
        choice = menu('', ['Play a new game', 'Continue last game', 'Quit'], 24)

        if choice == 0:  #new game
            race_choose()
        if choice == 1:  #load last game
            try:
                load_game()
            except:
                msgbox('\n No saved game to load.\n', 24)
                continue
            play_game()
        elif choice == 2:  #quit
            break

libtcod.console_set_custom_font('arial10x10.png', libtcod.FONT_TYPE_GREYSCALE | libtcod.FONT_LAYOUT_TCOD)
libtcod.console_init_root(SCREEN_WIDTH, SCREEN_HEIGHT, 'UZHOROD CAVES OF THE DEATH', False)
libtcod.sys_set_fps(LIMIT_FPS)
con = libtcod.console_new(MAP_WIDTH, MAP_HEIGHT)
panel = libtcod.console_new(SCREEN_WIDTH, PANEL_HEIGHT)
#AI SECTION
defense_funcs=[cast_heal]
zali=cast_damage_spell(damage=5,char='&',dur=1,effectname='fire',effect = 'burn up',rad=1,mana=0,color=libtcod.dark_orange)
print(cast_damage_spell.casting.__name__==zali.casting.__name__)
offensive_funcs=[cast_damage_spell.casting]
misc_funcs=[]




#AI CAN'T NOW HOW TO USE OBJECTS WITHOUT KNOWING THEIR CATEGORY, SO BE CLEVER
main_menu()
